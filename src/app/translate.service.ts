import { Injectable } from '@angular/core';
import { get } from 'lodash';

import enUS from '@app/i18n/en-US';
import ruRU from '@app/i18n/ru-RU';

interface Global {
  window: any;
}

@Injectable({
  providedIn: 'root'
})
export class TranslateService {

  private static locales = {
    'en-US': enUS,
    'ru-RU': ruRU,
  };

  private static localeString = 'en-US';
  private static locale = enUS;

  constructor() {
    const appLanguage = window.navigator.language;

    TranslateService.setLanguage(localStorage.getItem('locale') || appLanguage);
  }

  static getLanguageString(): string {
    return this.localeString;
  }

  static setLanguageString(locale: string): void {
    this.localeString = locale;
  }

  static setLanguage(locale: string): void {
    localStorage.setItem('locale', locale);
    TranslateService.setLanguageString(locale);
    TranslateService.locale = (TranslateService.locales[locale] || enUS);
  }

  get(path: string): string {
    return get(TranslateService.locale, path, path);
  }

}
