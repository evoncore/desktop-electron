import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'evoncore';
  loading = true;

  constructor(
    private apollo: Apollo,
  ) {}

  ngOnInit(): void {
    setTimeout(() => this.loading = false, 0);

    this.apollo
      .watchQuery({
        query: gql`
          query Profiles {
            profile(id: 1) {
              id,
              firstName,
              lastName
            }
          }
        `,
      })
      .valueChanges.subscribe(result => {
        console.log(result);
      });
  }

}
