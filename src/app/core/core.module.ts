import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { DragulaModule } from 'ng2-dragula';

// SERVICES //
import { PropertyService } from './components/property/property.service';
// SERVICES //

// PROPERTY //
import { PropertyComponent } from './components/property/property.component';
import { PropertyTextComponent } from './components/property/text/property-text.component';
import { PropertyLabelComponent } from './components/property/label/property-label.component';
import { PropertyPopoverComponent } from './components/property/popover/property-popover.component';
import { PropertyDateComponent } from './components/property/date/property-date.component';
import { PropertyCheckboxComponent } from './components/property/checkbox/property-checkbox.component';
// PROPERTY //

// PROPERTY CONTANINER //
import { PropertyContainerComponent } from './components/property-container/property-container.component';
// PROPERTY CONTAINER //

// PICON //
import { PiconComponent } from './components/picon/picon.component';
// PICON //

// CARD //
import { CardComponent } from './components/card/card.component';
// CARD //

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgZorroAntdModule,
    DragulaModule.forRoot(),
  ],
  declarations: [
    PropertyComponent,
    PropertyTextComponent,
    PropertyLabelComponent,
    PropertyPopoverComponent,
    PropertyDateComponent,
    PropertyCheckboxComponent,
    PropertyContainerComponent,
    PiconComponent,
    CardComponent,
  ],
  exports: [
    PropertyComponent,
    PropertyContainerComponent,
    PiconComponent,
  ],
  providers: [
    PropertyService,
  ],
})
export class CoreModule { }
