export default function(event, callback: (event: any) => void) {
  const keyCode = event.keyCode || event.which;

  if (keyCode === 13) {
    callback.call(this, event);
    return false;
  }
}
