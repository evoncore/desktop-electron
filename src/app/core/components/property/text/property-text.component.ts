import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputComponent } from '../../../base-components/input';

@Component({
  selector: 'app-property-text',
  templateUrl: './property-text.component.html',
  styleUrls: ['./property-text.component.less']
})
export class PropertyTextComponent extends InputComponent implements OnInit {

  changedValue: string;

  @Output() valueChange: EventEmitter<string> = new EventEmitter();

  @Input()
  get value(): string {
    return this.changedValue;
  }

  set value(value: string) {
    this.changedValue = value;
    this.valueChange.emit(this.changedValue);
  }

  constructor() {
    super();
  }

  ngOnInit() { }

}
