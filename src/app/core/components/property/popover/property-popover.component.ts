import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-property-popover',
  templateUrl: './property-popover.component.html',
  styleUrls: ['./property-popover.component.less']
})
export class PropertyPopoverComponent implements OnInit {

  changedVisible: boolean;

  @Input() content;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter();

  @Input()
  get visible(): boolean {
    return this.changedVisible;
  }

  set visible(visible: boolean) {
    this.changeVisible(visible);
  }

  constructor() { }

  ngOnInit() {}

  changeVisible(visible: boolean): void {
    this.changedVisible = visible;
    this.visibleChange.emit(this.changedVisible);
  }

  onVisibleChange(visible: boolean): void {
    this.changeVisible(visible);
  }

}
