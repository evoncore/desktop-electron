import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyPopoverComponent } from './property-popover.component';

describe('PropertyPopoverComponent', () => {
  let component: PropertyPopoverComponent;
  let fixture: ComponentFixture<PropertyPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
