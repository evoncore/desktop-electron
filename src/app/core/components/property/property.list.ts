import { Property, Types as PropertyTypes } from '../../interfaces/property.interface';

export default function(): Property[] {
  return [
    {
      type: PropertyTypes.text,
      label: {
        title: 'Текст',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: '',
    },
    {
      type: PropertyTypes.number,
      label: {
        title: 'Число',
        placeholder: 'Число',
        icon: 'hashtag',
      },
      value: '',
      disabled: true,
    },
    {
      type: PropertyTypes.select,
      label: {
        title: 'Меню',
        placeholder: 'Меню',
        icon: 'checklist',
      },
      value: [],
      disabled: true,
    },
    {
      type: PropertyTypes.tags,
      label: {
        title: 'Тэги',
        placeholder: 'Тэги',
        icon: 'tags',
      },
      value: [],
      disabled: true,
    },
    {
      type: PropertyTypes.date,
      label: {
        title: 'Дата',
        placeholder: 'Дата',
        icon: 'calendar',
        settings: {
          title: 'Редактировать',
        },
      },
      value: null,
    },
    {
      type: PropertyTypes.users,
      label: {
        title: 'Пользователи',
        placeholder: 'Пользователи',
        icon: 'user-group-man-woman',
      },
      value: [],
      disabled: true,
    },
    {
      type: PropertyTypes.files,
      label: {
        title: 'Файлы',
        placeholder: 'Файлы',
        icon: 'copy',
      },
      value: [],
      disabled: true,
    },
    {
      type: PropertyTypes.checkbox,
      label: {
        title: 'Флажок',
        placeholder: 'Флажок',
        icon: 'checked',
      },
      value: false,
    },
    {
      type: PropertyTypes.url,
      label: {
        title: 'Гиперссылка',
        placeholder: 'Гиперссылка',
        icon: 'link',
        settings: {
          title: 'Выбрать иконку',
          enableIconsPicker: true,
        },
      },
      value: '',
    },
    {
      type: PropertyTypes.email,
      label: {
        title: 'E-mail',
        placeholder: 'E-mail',
        icon: 'email',
      },
      value: '',
    },
    {
      type: PropertyTypes.phone,
      label: {
        title: 'Телефон',
        placeholder: 'Телефон',
        icon: 'phone',
      },
      value: '',
    },
  ];
}
