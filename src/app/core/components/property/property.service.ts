import { Injectable } from '@angular/core';
import { Property } from '../../interfaces/property.interface';
import propertyList from './property.list';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  defaultIndex = 0;

  constructor() {}

  getDefeaultProperty(): Property {
    return propertyList()[this.defaultIndex];
  }

  getPropertyList(): Property[] {
    return propertyList();
  }

}
