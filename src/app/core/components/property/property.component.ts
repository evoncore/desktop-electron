import { Component, OnInit, Input } from '@angular/core';
import { Property, Types as PropertyTypes } from '../../interfaces/property.interface';
import { isEqual, isEmpty } from 'lodash';
import { PropertyService } from '@app/core/components/property/property.service';

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.less']
})
export class PropertyComponent implements OnInit {

  @Input() property: Property;
  @Input() hasContainer = false;
  @Input() withIcon = true;
  @Input() withLabel = true;
  @Input() showLabel = false;

  types = PropertyTypes;

  constructor(
    private propertyService: PropertyService
  ) {}

  ngOnInit() {
    if (isEmpty(this.property)) {
      this.property = this.propertyService.getDefeaultProperty();
    }
  }

  onSubmit(): void {
    console.log(this.property);
  }

  onChangeProperty(nextProperty: Property): void {
    if (isEqual(this.property, nextProperty)) {
      return;
    }

    if (
      this.property.label.title !== this.property.label.placeholder &&
      this.property.label.title !== nextProperty.label.placeholder
    ) {
      nextProperty.label.title = this.property.label.title;
    } else {
      nextProperty.label.title = nextProperty.label.placeholder;
    }

    this.property = nextProperty;
  }

}
