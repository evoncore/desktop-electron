import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-property-checkbox',
  templateUrl: './property-checkbox.component.html',
  styleUrls: ['./property-checkbox.component.less']
})
export class PropertyCheckboxComponent implements OnInit {

  changedValue: string;

  @Output() valueChange: EventEmitter<string> = new EventEmitter();

  @Input()
  get value(): string {
    return this.changedValue;
  }

  set value(value: string) {
    this.changedValue = value;
    this.valueChange.emit(this.changedValue);
  }

  constructor() {}

  ngOnInit() {}

}
