import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyCheckboxComponent } from './property-checkbox.component';

describe('PropertyCheckboxComponent', () => {
  let component: PropertyCheckboxComponent;
  let fixture: ComponentFixture<PropertyCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
