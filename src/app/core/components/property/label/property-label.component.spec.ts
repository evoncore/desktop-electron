import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyLabelComponent } from './property-label.component';

describe('PropertyLabelComponent', () => {
  let component: PropertyLabelComponent;
  let fixture: ComponentFixture<PropertyLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
