import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputComponent } from '../../../base-components/input';
import { Property } from '../../../interfaces/property.interface';
import { PropertyService } from '@app/core/components/property/property.service';

@Component({
  selector: 'app-property-label',
  templateUrl: './property-label.component.html',
  styleUrls: ['./property-label.component.less']
})
export class PropertyLabelComponent extends InputComponent implements OnInit {

  properties: Property[];
  popupInput: string;
  visible = false;

  @Input() property;
  @Input() hasContainer: boolean;
  @Input() withIcon: boolean;
  @Input() withLabel: boolean;
  @Input() showLabel: boolean;
  @Output() changeProperty: EventEmitter<Property> = new EventEmitter();

  constructor(
    private propertyService: PropertyService
  ) {
    super();
  }

  ngOnInit() {
    this.properties = this.propertyService.getPropertyList();
    this.popupInput = this.property.label.title;
  }

  onBlur(): void {
    this.property.label.title = this.popupInput;
  }

  onPressEnter(): void {
    this.property.label.title = this.popupInput;
    this.visible = false;
  }

  onSelectProperty(property: Property): void {
    this.changeProperty.emit(property);
    this.visible = false;
  }

}
