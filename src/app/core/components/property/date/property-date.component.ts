import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-property-date',
  templateUrl: './property-date.component.html',
  styleUrls: ['./property-date.component.less']
})
export class PropertyDateComponent implements OnInit {

  changedValue: string;

  @Output() valueChange: EventEmitter<string> = new EventEmitter();

  @Input()
  get value(): string {
    return this.changedValue;
  }

  set value(value: string) {
    this.changedValue = value;
    this.valueChange.emit(this.changedValue);
  }

  constructor() {}

  ngOnInit() { }

}
