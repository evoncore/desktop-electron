import { Component, OnInit, Input } from '@angular/core';
import { Property } from '../../interfaces/property.interface';
import { PropertyService } from '@app/core/components/property/property.service';

@Component({
  selector: 'app-property-container',
  templateUrl: './property-container.component.html',
  styleUrls: ['./property-container.component.less']
})
export class PropertyContainerComponent implements OnInit {

  @Input() properties: Property[];

  constructor(
    private propertyService: PropertyService
  ) { }

  ngOnInit() {}

}
