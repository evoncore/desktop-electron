import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-picon',
  templateUrl: './picon.component.html',
  styleUrls: ['./picon.component.less']
})
export class PiconComponent implements OnInit {

  @Input() piconClass: string;
  @Input() type = 'dusk'; // dusk // office
  @Input() name: string;
  @Input() size = 15;

  constructor() { }

  ngOnInit() { }

}
