import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiconComponent } from './picon.component';

describe('PiconComponent', () => {
  let component: PiconComponent;
  let fixture: ComponentFixture<PiconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
