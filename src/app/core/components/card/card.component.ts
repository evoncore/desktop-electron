import { Component, OnInit } from '@angular/core';
import { Property, Types as PropertyTypes } from '../../interfaces/property.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  properties: Property[] = [
    {
      type: PropertyTypes.text,
      label: {
        title: 'Текст',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: '',
    },
    {
      type: PropertyTypes.text,
      label: {
        title: 'Текст',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: '',
    },
    {
      type: PropertyTypes.text,
      label: {
        title: 'Текст',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: '',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
