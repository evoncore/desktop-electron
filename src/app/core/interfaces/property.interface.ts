export enum Types {
  'text',
  'number',
  'select',
  'tags',
  'date',
  'users',
  'files',
  'checkbox',
  'url',
  'email',
  'phone',
}

export interface PropertySettings {
  title: string;
  enableIconsPicker?: boolean;
}

export interface PropertyLabel {
  title: string;
  placeholder: string;
  icon: string;
  settings?: PropertySettings;
}

export interface Property {
  type: Types;
  label: PropertyLabel;
  value: string|boolean|any[]|null;
  disabled?: boolean;
}
