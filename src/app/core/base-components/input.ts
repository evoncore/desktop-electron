import { Output, EventEmitter } from '@angular/core';
import { pressEnter } from '../helpers/events';

export class InputComponent {

  onPressEnterHelper;
  @Output() pressenter: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.onPressEnterHelper = pressEnter;
  }

  onPressEnter(event): void {
    this.pressenter.emit(event);
    event.target.blur();
  }

}
