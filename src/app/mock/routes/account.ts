import { HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http';
import { of, throwError, Observable } from 'rxjs';

const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const REGEX_PHONE_NUMBER = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

export default function(request: HttpRequest<any>): Observable<HttpEvent<any>> {
  const users = JSON.parse(localStorage.getItem('users')) || [];
  const profiles = JSON.parse(localStorage.getItem('profiles')) || [];

  if (request.url.endsWith('/account/login') && request.method === 'POST') {
    const bodyUser = request.body;
    const filteredUsers = users.filter(user => {
      return (
        user.email === bodyUser.login ||
        user.phoneNumber === bodyUser.login ||
        user.userName === bodyUser.login
      ) && user.password === bodyUser.password;
    });

    if (bodyUser.login === '') {
      return throwError({ status: 400, body: { error: 'Enter Email, Phone number or Username.' } });
    }

    if (
      bodyUser.login.length < 3 &&
      !REGEX_EMAIL.test(bodyUser.email) &&
      !REGEX_PHONE_NUMBER.test(bodyUser.phoneNumber)
    ) {
      return throwError({ status: 400, body: { error: 'Incorrect login.' } });
    }

    if (bodyUser.password === '') {
      return throwError({ status: 400, body: { error: 'Enter password.' } });
    }

    if (filteredUsers.length) {
      const user = filteredUsers[0];

      if (user.password !== bodyUser.password) {
        return throwError({ status: 400, body: { error: 'Incorrect login or password.' } });
      }

      const body = {
        user,
        token: 'mock-jwt-token'
      };

      return of(new HttpResponse({ status: 200, body }));
    }

    return throwError({ status: 400, body: { error: 'Incorrect login or password.' }, });
  }

  if (request.url.endsWith('/account/register/step-1') && request.method === 'POST') {
    const newUser = request.body;

    if (newUser.userName.length === 0) {
      return throwError({ status: 400, body: { error: 'Username cannot be empty.' } });
    }

    if (newUser.userName.length < 3) {
      return throwError({ status: 400, body: { error: 'Username length should be 3 symbols or more.' } });
    }

    if (users.filter(user => user.userName === newUser.userName).length) {
      return throwError({ status: 400, body: { error: `Login "${newUser.login}" already in use.` } });
    }

    if (newUser.email.length === 0) {
      return throwError({ status: 400, body: { error: 'Email cannot be empty.' } });
    }

    if (!REGEX_EMAIL.test(newUser.email)) {
      return throwError({ status: 400, body: { error: 'Incorrect Email.' } });
    }

    if (users.filter(user => user.email === newUser.email).length) {
      return throwError({ status: 400, body: { error: `Login "${newUser.login}" already in use.` } });
    }

    if (newUser.phoneNumber.length === 0) {
      return throwError({ status: 400, body: { error: 'Phone number cannot be empty.' } });
    }

    if (!REGEX_PHONE_NUMBER.test(newUser.phoneNumber)) {
      return throwError({ status: 400, body: { error: 'Incorrect Phone number.' } });
    }

    if (users.filter(user => user.phoneNumber === newUser.phoneNumber).length) {
      return throwError({ status: 400, body: { error: `Login "${newUser.login}" already in use.` } });
    }

    if (newUser.password === '') {
      return throwError({ status: 400, body: { error: 'Password cannot be empty.' } });
    }

    if (newUser.password.length < 6) {
      return throwError({ status: 400, body: { error: 'Unsecure password. Password length should be 6 or more.' } });
    }

    if (newUser.repeatPassword === '') {
      return throwError({ status: 400, body: { error: 'Please, repeat password.' } });
    }

    if (newUser.password !== newUser.repeatPassword) {
      return throwError({ status: 400, body: { error: 'Password do not match.' } });
    }

    return of(new HttpResponse({ status: 200 }));
  }

  if (request.url.endsWith('/account/register/step-2') && request.method === 'POST') {
    const newProfile = request.body;

    if (newProfile.firstName === '') {
      return throwError({ status: 400, body: { error: 'First Name cannot be empty.' } });
    }

    if (newProfile.lastName === '') {
      return throwError({ status: 400, body: { error: 'Last Name cannot be empty.' } });
    }

    if (newProfile.country === '') {
      return throwError({ status: 400, body: { error: 'Country cannot be empty.' } });
    }

    if (newProfile.gender === '') {
      return throwError({ status: 400, body: { error: 'Gender cannot be empty.' } });
    }

    return of(new HttpResponse({ status: 200 }));
  }

  if (request.url.endsWith('/account/register/step-3') && request.method === 'POST') {
    const {user, profile} = request.body;

    user.id = (users.length + 1);
    users.push(user);

    profile.id = (profiles.length + 1);
    profiles.push(profile);

    localStorage.setItem('users', JSON.stringify(users));
    localStorage.setItem('profiles', JSON.stringify(profiles));

    return of(new HttpResponse({ status: 200, body: { token: 'mock-jwt-token' } }));
  }

  return throwError({ status: 404 });
}
