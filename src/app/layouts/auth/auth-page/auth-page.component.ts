import { Component, OnInit } from '@angular/core';
import { fadeAnimation } from '@app/animations';
import { TranslateService } from '@app/translate.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.less'],
  animations: [fadeAnimation],
})
export class AuthPageComponent implements OnInit {

  locale: string;
  showAuth = false;
  isLogin = true;
  localize;

  constructor(private translate: TranslateService) {
    this.locale = TranslateService.getLanguageString();
    this.localize = this.translate.get;
  }

  ngOnInit() {}

  onChangeLanguage(key): void {
    TranslateService.setLanguage(key);
  }

}
