import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterStepProfileComponent } from './register-step-profile.component';

describe('RegisterStepProfileComponent', () => {
  let component: RegisterStepProfileComponent;
  let fixture: ComponentFixture<RegisterStepProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterStepProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterStepProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
