import { Component } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { TranslateService } from '@app/translate.service';
import { AuthService } from '@app/layouts/auth/auth.service';
import { AuthPage } from '@app/layouts/auth/auth-page';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-register-step-profile',
  templateUrl: './register-step-profile.component.html',
  styleUrls: ['./register-step-profile.component.less']
})
export class RegisterStepProfileComponent extends AuthPage {

  storagePrefix = 'accountProfile';
  form: any = {
    firstName: '',
    lastName: '',
    country: '',
    birthday: '',
    gender: '',
  };

  constructor(
    protected router: Router,
    protected notification: NzNotificationService,
    protected authService: AuthService,
    protected translate: TranslateService,
  ) {
    super(router, notification, authService, translate);
  }

  onNext(): void {
    this.loading = true;
    this.trimForm();
    this.authService
      .registerStepTwo(this.form)
      .subscribe(() => {
        this.loading = false;

        if (!isEmpty(this.notificationMessageError)) {
          this.notification.remove(this.notificationMessageError.messageId);
        }

        this.router.navigate(['/auth/register/step-3']);
      },
      ({body}) => {
        this.loading = false;
        this.notificationMessageError = this.notification.error(this.localize('register.error'), body.error);
      });
  }

}
