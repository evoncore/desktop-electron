import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { isEmpty } from 'lodash';
import { NzNotificationService } from 'ng-zorro-antd';
import { AuthService } from '@app/layouts/auth/auth.service';
import { TranslateService } from '@app/translate.service';
import { AuthPage } from '@app/layouts/auth/auth-page';

@Component({
  selector: 'app-register-step-user',
  templateUrl: './register-step-user.component.html',
  styleUrls: ['./register-step-user.component.less']
})
export class RegisterStepUserComponent extends AuthPage {

  storagePrefix = 'accountUser';
  form: any = {
    userName: '',
    email: '',
    phoneNumber: '',
    password: '',
    repeatPassword: '',
  };

  constructor(
    protected router: Router,
    protected notification: NzNotificationService,
    protected authService: AuthService,
    protected translate: TranslateService,
  ) {
    super(router, notification, authService, translate);
  }

  onNext(): void {
    this.loading = true;
    this.trimForm();
    this.authService
      .registerStepOne(this.form)
      .subscribe(() => {
        this.loading = false;

        if (!isEmpty(this.notificationMessageError)) {
          this.notification.remove(this.notificationMessageError.messageId);
        }

        this.router.navigate(['/auth/register/step-2']);
      },
      ({body}) => {
        this.loading = false;
        this.notificationMessageError = this.notification.error(this.localize('register.error'), body.error);
      });
  }

}
