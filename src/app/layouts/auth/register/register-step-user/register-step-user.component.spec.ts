import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterStepUserComponent } from './register-step-user.component';

describe('RegisterStepUserComponent', () => {
  let component: RegisterStepUserComponent;
  let fixture: ComponentFixture<RegisterStepUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterStepUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterStepUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
