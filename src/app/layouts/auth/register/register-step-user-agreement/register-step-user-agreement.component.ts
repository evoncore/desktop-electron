import { Component, OnInit } from '@angular/core';
import { NzNotificationService, NzNotificationDataFilled } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { TranslateService } from '@app/translate.service';
import { AuthService } from '@app/layouts/auth/auth.service';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-register-step-user-agreement',
  templateUrl: './register-step-user-agreement.component.html',
  styleUrls: ['./register-step-user-agreement.component.less']
})
export class RegisterStepUserAgreementComponent implements OnInit {

  notificationMessageError: NzNotificationDataFilled;
  checked = false;
  loading = false;
  localize;

  constructor(
    private router: Router,
    private notification: NzNotificationService,
    private authService: AuthService,
    private translate: TranslateService,
  ) {
    this.localize = this.translate.get;
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.checked) {
      this.loading = true;

      const data = {
        user: JSON.parse(localStorage.getItem('accountUser')),
        profile: JSON.parse(localStorage.getItem('accountProfile')),
      };

      this.authService
        .registerStepThree(data)
        .subscribe(() => {
          this.loading = false;

          localStorage.removeItem('accountUser');
          localStorage.removeItem('accountProfile');

          if (!isEmpty(this.notificationMessageError)) {
            this.notification.remove(this.notificationMessageError.messageId);
          }
          
          this.router.navigate(['/personal-area/profile']);
        },
        ({body}) => {
          this.loading = false;
          this.notificationMessageError = this.notification.error(this.localize('register.error'), body.error);
        });
    }
  }

}
