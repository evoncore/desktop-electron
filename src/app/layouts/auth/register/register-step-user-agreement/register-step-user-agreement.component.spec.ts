import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterStepUserAgreementComponent } from './register-step-user-agreement.component';

describe('RegisterStepUserAgreementComponent', () => {
  let component: RegisterStepUserAgreementComponent;
  let fixture: ComponentFixture<RegisterStepUserAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterStepUserAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterStepUserAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
