import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@app/translate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept-Language': TranslateService.getLanguageString(),
    });
  }

  login(form) {
    return this.http.post('/account/login', form, { headers: this.getHeaders() });
  }

  registerStepOne(form) {
    return this.http.post('/account/register/step-1', form, { headers: this.getHeaders() });
  }

  registerStepTwo(form) {
    return this.http.post('/account/register/step-2', form, { headers: this.getHeaders() });
  }

  registerStepThree(form) {
    return this.http.post('/account/register/step-3', form, { headers: this.getHeaders() });
  }

  register(form) {
    return this.http.post('/account/register', form, { headers: this.getHeaders() });
  }

}
