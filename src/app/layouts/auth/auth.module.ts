import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgZorroAntdModule, NZ_NOTIFICATION_CONFIG } from 'ng-zorro-antd';

import { LoginComponent } from '@app/layouts/auth/login/login.component';
import { AuthPageComponent } from '@app/layouts/auth/auth-page/auth-page.component';
import { RegisterStepUserComponent } from '@app/layouts/auth/register/register-step-user/register-step-user.component';
import { RegisterStepProfileComponent } from '@app/layouts/auth/register/register-step-profile/register-step-profile.component';
import { RegisterStepUserAgreementComponent } from '@app/layouts/auth/register/register-step-user-agreement/register-step-user-agreement.component';

const routes: Routes = [
  { path: 'auth', component: AuthPageComponent, children: [
    { path: 'register/step-1', component: RegisterStepUserComponent },
    { path: 'register/step-2', component: RegisterStepProfileComponent },
    { path: 'register/step-3', component: RegisterStepUserAgreementComponent },
    { path: 'login', component: LoginComponent },
  ] },
];

@NgModule({
  declarations: [
    AuthPageComponent,
    RegisterStepUserComponent,
    RegisterStepProfileComponent,
    RegisterStepUserAgreementComponent,
    LoginComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    NgZorroAntdModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: NZ_NOTIFICATION_CONFIG, useValue: { nzDuration: 10000, nzMaxStack: 1 } }
  ]
})
export class AuthModule { }
