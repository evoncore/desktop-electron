import { Router } from '@angular/router';
import { NzNotificationService, NzNotificationDataFilled } from 'ng-zorro-antd';
import { AuthService } from '@app/layouts/auth/auth.service';
import { TranslateService } from '@app/translate.service';
import { omit, clone, isEmpty, forEach } from 'lodash';
import { OnInit } from '@angular/core';

export class AuthPage implements OnInit {
  storagePrefix;
  notificationMessageError: NzNotificationDataFilled;
  loading = false;
  localize;
  form = {};

  constructor(
    protected router: Router,
    protected notification: NzNotificationService,
    protected authService: AuthService,
    protected translate: TranslateService,
  ) {
    this.localize = this.translate.get;
  }

  trimForm() {
    forEach(this.form, (value: string, key: string) => this.form[key] = value.trim());
  }

  ngOnInit() {
    const data = localStorage.getItem(this.storagePrefix);

    if (!isEmpty(data)) {
      this.form = JSON.parse(data);
    }
  }

  onChange(value, type) {
    if (isEmpty(this.storagePrefix) || isEmpty(type) || isEmpty(value)) {
      return;
    }

    const data = omit(clone(this.form), 'repeatPassword');
    data[type] = value;
    localStorage.setItem(this.storagePrefix, JSON.stringify(data));
  }
}
