import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/layouts/auth/auth.service';
import { NzNotificationService, NzNotificationDataFilled } from 'ng-zorro-antd';
import { TranslateService } from '@app/translate.service';
import { AuthPage } from '@app/layouts/auth/auth-page';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent extends AuthPage {

  notificationMessage: NzNotificationDataFilled;
  errorsNotificationMessage: NzNotificationDataFilled[] = [];
  loading = false;
  localize;
  form: any = {
    login: '',
    password: '',
  };

  constructor(
    protected router: Router,
    protected notification: NzNotificationService,
    protected authService: AuthService,
    protected translate: TranslateService,
  ) {
    super(router, notification, authService, translate);
  }

  onSubmit(): void {
    this.loading = true;
    this.trimForm();
    this.authService
      .login(this.form)
      .subscribe(() => {
        this.loading = false;

        if (!isEmpty(this.notificationMessage)) {
          this.notification.remove(this.notificationMessage.messageId);
        }

        this.router.navigate(['/personal-area/profile']);
      },
      ({body}) => {
        this.loading = false;

        if (!isEmpty(this.notificationMessage)) {
          this.notification.remove(this.notificationMessage.messageId);
        }

        this.notificationMessage = this.notification.error(this.localize('login.error'), body.error);
      });
  }

}
