import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  companyMenu = false;
  siderOpened = false;

  constructor() {
    this.companyMenu = (localStorage.getItem('userSettingsCompanyMenu') === '1');
    this.siderOpened = (localStorage.getItem('userSettingsSiderOpened') === '1');
  }

  showCompanyMenu() {
    localStorage.setItem('userSettingsCompanyMenu', '1');
    this.companyMenu = true;
  }

  hideCompanyMenu() {
    localStorage.setItem('userSettingsCompanyMenu', '0');
    this.companyMenu = false;
  }

  showSider() {
    localStorage.setItem('userSettingsSiderOpened', '1');
    this.siderOpened = true;
  }

  hideSider() {
    localStorage.setItem('userSettingsSiderOpened', '0');
    this.siderOpened = false;
  }

  toggleSider() {
    if (this.siderOpened) {
      this.hideSider();
    } else {
      this.showSider();
    }
  }

}
