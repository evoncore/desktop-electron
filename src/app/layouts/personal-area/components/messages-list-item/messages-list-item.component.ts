import { Component, OnInit, Input } from '@angular/core';
import { timestamp } from 'rxjs/operators';

@Component({
  selector: 'app-messages-list-item',
  templateUrl: './messages-list-item.component.html',
  styleUrls: ['./messages-list-item.component.less']
})
export class MessagesListItemComponent implements OnInit {

  @Input() title;
  @Input() timestamp;
  @Input() lastMessage;
  @Input() lastMessageUserAvatar;
  @Input() badgeCount;

  constructor() { }

  ngOnInit() {
  }

}
