import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notification-message',
  templateUrl: './notification-message.component.html',
  styleUrls: ['./notification-message.component.less']
})
export class NotificationMessageComponent implements OnInit {

  @Input() title;
  @Input() icon;
  @Input() timestamp;

  constructor() { }

  ngOnInit() {
  }

}
