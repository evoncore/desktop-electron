import { PersonalAreaModule } from './personal-area.module';

describe('PersonalAreaModule', () => {
  let personalAreaModule: PersonalAreaModule;

  beforeEach(() => {
    personalAreaModule = new PersonalAreaModule();
  });

  it('should create an instance', () => {
    expect(personalAreaModule).toBeTruthy();
  });
});
