import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { NgZorroAntdModule, NZ_NOTIFICATION_CONFIG } from 'ng-zorro-antd';
import { CoreModule } from '@app/core/core.module';
import { RootComponent } from '@app/layouts/personal-area/pages/root/root.component';
import { ProfileComponent } from '@app/layouts/personal-area/pages/profile/profile.component';
import { MessagesComponent } from '@app/layouts/personal-area/pages/messages/messages.component';
import { JobsComponent } from '@app/layouts/personal-area/pages/jobs/jobs.component';
import { NotificationMessageComponent } from './components/notification-message/notification-message.component';
import { ContactsComponent } from '@app/layouts/personal-area/pages/contacts/contacts.component';
import { CompanyComponent } from '@app/layouts/personal-area/pages/company/company.component';
import { CreateCompanyComponent } from '@app/layouts/personal-area/pages/create-company/create-company.component';
import { CompanyLocationComponent } from './components/company-location/company-location.component';
import { ManageComponent } from './pages/company/sub-pages/manage/manage.component';
import { DashboardComponent } from './pages/company/sub-pages/dashboard/dashboard.component';
import { MessagesListItemComponent } from './components/messages-list-item/messages-list-item.component';

const routes: Routes = [
  { path: 'personal-area', component: RootComponent, children: [
    { path: 'messages', component: MessagesComponent },
    { path: 'contacts', component: ContactsComponent },
    { path: 'jobs', component: JobsComponent },
    { path: 'company', component: CompanyComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'manage', component: ManageComponent },
    ] },
    { path: 'create-company', component: CreateCompanyComponent },
    { path: 'profile', component: ProfileComponent },
  ] },
];

@NgModule({
  declarations: [
    RootComponent,
    MessagesComponent,
    JobsComponent,
    ProfileComponent,
    NotificationMessageComponent,
    ContactsComponent,
    CompanyComponent,
    CreateCompanyComponent,
    CompanyLocationComponent,
    ManageComponent,
    DashboardComponent,
    MessagesListItemComponent,
  ],
  imports: [
    CoreModule,
    RouterModule.forRoot(routes),
    FormsModule,
    NgZorroAntdModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: NZ_NOTIFICATION_CONFIG, useValue: { nzDuration: 3000 } }
  ]
})
export class PersonalAreaModule { }
