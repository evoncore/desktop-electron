import { Component, OnInit } from '@angular/core';
import { Location, PlatformLocation } from '@angular/common';
import { fadeAnimation } from '@app/animations';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { NavigationService } from '@app/layouts/personal-area/navigation.service';

const ACTION_POP = 'POP';
const ACTION_PUSH = 'PUSH';

@Component({
  selector: 'app-personal-area-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.less'],
  animations: [fadeAnimation],
})
export class RootComponent implements OnInit {

  notificationsVisible = false;
  settingsVisible = false;

  locationHistory = [];
  locationHistoryIndex = 0;
  action: string;
  prevAction: string;

  historyNavigationBackDisabled = true;
  historyNavigationNextDisabled = true;

  firstName = 'Vladimir';
  lastName = 'Sentiurin';
  position = 'Software Engineer';
  currentYear;

  constructor(
    protected router: Router,
    protected location: Location,
    protected platformLocation: PlatformLocation,
    protected navigationService: NavigationService,
  ) {}

  ngOnInit() {
    this.currentYear = new Date().getFullYear();
    this.locationHistory.push(this.platformLocation.pathname);
    this.router.events.subscribe((route: RouterEvent) => {
      if (route instanceof NavigationEnd) {
        this.setNavigationButtonsDisabled();

        if (this.action !== ACTION_POP && this.locationHistoryIndex === 0 && this.prevAction === ACTION_POP) {
          this.action = undefined;
          this.prevAction = undefined;
          this.locationHistory = [this.locationHistory[0], route.url];
          this.locationHistoryIndex = (this.locationHistory.length - 1);

          this.setNavigationButtonsDisabled();
          return;
        }

        if ((this.prevAction === ACTION_POP || this.prevAction === ACTION_PUSH) && !this.action) {
          this.action = undefined;
          this.prevAction = undefined;
          this.locationHistory = this.locationHistory.slice(0, this.locationHistoryIndex + 1);
          this.locationHistory.push(route.url);
          this.locationHistoryIndex = (this.locationHistory.length - 1);

          this.setNavigationButtonsDisabled();
          return;
        }

        if (this.action === ACTION_POP) {
          this.prevAction = this.action;
          this.action = undefined;
          return;
        }

        if (this.action === ACTION_PUSH) {
          this.prevAction = this.action;
          this.action = undefined;
          return;
        }

        this.locationHistory.push(route.url);
        this.locationHistoryIndex = (this.locationHistory.length - 1);
      }
    });
  }

  private setNavigationButtonsDisabled() {
    if (this.locationHistory.length === 1) {
      this.historyNavigationBackDisabled = false;
    } else {
      this.historyNavigationBackDisabled = (this.locationHistoryIndex === 0);
    }

    this.historyNavigationNextDisabled = (this.locationHistoryIndex >= (this.locationHistory.length - 1));
  }

  onHistoryBack(): void {
    this.action = ACTION_POP;

    if (!this.historyNavigationBackDisabled) {
      this.locationHistoryIndex--;
      this.router.navigate([this.locationHistory[this.locationHistoryIndex]]);
    }
  }

  onHistoryNext(): void {
    this.action = ACTION_PUSH;

    if (!this.historyNavigationNextDisabled) {
      this.locationHistoryIndex++;
      this.router.navigate([this.locationHistory[this.locationHistoryIndex]]);
    }
  }

  onNotifications(): void {
    this.notificationsVisible = true;
  }

  onSettings(): void {
    this.settingsVisible = true;
  }

  onCompanyMenuBack(): void {
    this.navigationService.hideCompanyMenu();
  }

  onCompany(): void {
    this.navigationService.showCompanyMenu();
  }

  onToggleSider(): void {
    this.navigationService.toggleSider();
  }

}
