import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { fadeAnimation } from '@app/animations';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.less'],
  animations: [fadeAnimation],
})
export class CompanyComponent implements OnInit {

  locationsCount = 3;
  employeesCount = 54;
  projectsCount = 24;
  teamsCount = 8;
  hiddenManage = false;

  constructor(
    protected router: Router,
    protected platformLocation: PlatformLocation,
  ) { }

  ngOnInit() {
    this.switchHeaderButton(this.platformLocation.pathname);
    this.router.events.subscribe((route: RouterEvent) => {
      if (route instanceof NavigationEnd) {
        this.switchHeaderButton(route.url);
      }
    });
  }

  switchHeaderButton(url: string) {
    this.hiddenManage = url.endsWith('/manage');
  }

}
