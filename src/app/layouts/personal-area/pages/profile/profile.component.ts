import { Component, OnInit } from '@angular/core';
import { Property, Types as PropertyTypes } from '@app/core/interfaces/property.interface';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  contacts: Property[] = [
    {
      type: PropertyTypes.text,
      label: {
        title: 'Email',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: 'volodymyr.sentiurin@gmail.com',
    },
    {
      type: PropertyTypes.text,
      label: {
        title: 'Phone number',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: '+380931428646',
    },
  ];

  socials: Property[] = [
    {
      type: PropertyTypes.url,
      label: {
        title: 'Facebook',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: 'fb.com/mr.sentiurin',
    },
    {
      type: PropertyTypes.url,
      label: {
        title: 'Instagram',
        placeholder: 'Текст',
        icon: 'text',
      },
      value: 'instagram.com/mr.sentiurin',
    },
    {
      type: PropertyTypes.url,
      label: {
        title: 'Telegram',
        placeholder: 'Текст',
        icon: 'instagram.com/mr.sentiurin',
      },
      value: '@DrunkAztec',
    },
  ];

  isEdit = false;

  firstName = 'Vladimir';
  lastName = 'Sentiurin';
  position = 'Software Engineer';
  contactsCount = 94;
  experienceCount = 3;
  awardsCount = 2;
  description = `He has piercing, deep-set brass eyes, a turned-up nose, and a square jaw. His, curly, red hair is worn in long braids.`;

  constructor(
    protected notification: NzNotificationService,
  ) {}

  ngOnInit() {
  }

  onEdit(): void {
    this.isEdit = true;
  }

  onSave(template): void {
    this.isEdit = false;
    this.notification.template(template, { nzClass: 'personal-area-notification personal-area-notification-single' });
  }

}
