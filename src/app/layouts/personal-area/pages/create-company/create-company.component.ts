import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.less']
})
export class CreateCompanyComponent implements OnInit {

  companyName = 'God Bless San Francisco';
  companyAbbreviation = 'GBSFO';
  companySlogan: string;
  companyDescription = 'GBSFO is a custom service provider, whose general office located in San Francisco, California. Location “San Francisco”, means, we working with Cisco… also our clients was: Yahoo, ExtremeNetworks, Total Coach and LinkedIn. Presently we actively working with LiveAction and Cisco.';

  current = 0;
  index = 'First-content';

  constructor() { }

  ngOnInit() {
  }

  skip(): void {
    this.current = 4;
  }

  pre(): void {
    this.current -= 1;
  }

  next(): void {
    this.current += 1;
  }

  done(): void {
    console.log('done');
  }

}
