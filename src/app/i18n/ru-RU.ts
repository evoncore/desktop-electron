export default {
  'submit': 'Отправить',
  'create': 'Создать',
  'authPage': {
    'title': 'Аккаунт',
    'loginButton': 'Войти',
    'registrationButton': 'Создать',
  },
  'login': {
    'error': 'Ошибка входа',
    'title': 'Войти:',
    'form': {
      'login': 'Эл. почта, Телефон или Имя пользователя',
      'password': 'Пароль',
    },
  },
  'register': {
    'error': 'Ошибка регистрации',
    'step-user': {
      'title': 'Заполните информацию:',
      'form': {
        'userName': 'Имя пользователя',
        'email': 'Эл. почта',
        'phoneNumber': 'Телефон',
        'password': 'Пароль',
        'repeatPassword': 'Повторите пароль',
        'passwordsDoNotMatch': 'Пароли не совпадают',
      },
    },
    'step-profile': {
      'title': 'Заполните профиль:',
      'form': {
        'firstName': 'Имя',
        'lastName': 'Фамилия',
        'country': 'Страна',
        'birthday': 'День рождения',
        'gender': 'Пол',
      },
    },
    'step-user-agreement': {
      'title': 'Пользовательское соглашение:',
      'form': {
        'checkbox': 'Я принимаю условия политики конфиденциальности',
      },
    },
  },
};
