export default {
  'submit': 'Submit',
  'create': 'Create',
  'authPage': {
    'title': 'Account',
    'loginButton': 'Sign in',
    'registrationButton': 'Create',
  },
  'login': {
    'error': 'Login error',
    'title': 'Sign in:',
    'form': {
      'login': 'Email, Phone number or Username',
      'password': 'Password',
    },
  },
  'register': {
    'error': 'Registration error',
    'step-user': {
      'title': 'Enter information:',
      'form': {
        'userName': 'Username',
        'email': 'Email',
        'phoneNumber': 'Phone number',
        'password': 'Password',
        'repeatPassword': 'Repeat password',
        'passwordsDoNotMatch': 'Passwords do not match',
      },
    },
    'step-profile': {
      'title': 'Enter profile:',
      'form': {
        'firstName': 'First Name',
        'lastName': 'Last Name',
        'country': 'Country',
        'birthday': 'Your date of birth',
        'gender': 'Gender',
      },
    },
    'step-user-agreement': {
      'title': 'User Agreement:',
      'form': {
        'checkbox': 'I accept the terms of Privacy Policy',
      },
    },
  },
};
