var electronInstaller = require('electron-winstaller');

resultPromise = electronInstaller.createWindowsInstaller({
  appDirectory: path.join(__dirname, 'build/win-unpacked'),
  outputDirectory: path.join(__dirname, 'build/win-installer'),
  authors: 'Evoncore Inc.',
  exe: 'evoncore.exe'
});

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));