const {app, BrowserWindow, Menu} = require('electron');
const path = require('path')
  
function createWindow () {
  const win = new BrowserWindow({
    // titleBarStyle: 'hidden',
    title: 'Evoncore',
    width: 1199, height: 840,
    minWidth: 1199, minHeight: 840,
    // resizable: false,
    // frame: false,
    // fullscreen: true,
    // fullscreenable: false,
    // transparent: true,
    icon: path.join(__dirname, 'logo.png'),
    backgroundColor: '#232931',
  });

  const menuTemplate = [
    {
      label: 'Evoncore',
    }
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(menuTemplate));

  win.loadURL('http://localhost:4200');
  win.webContents.openDevTools({detach:true});
  // win.webContents.on("devtools-opened", () => { win.webContents.closeDevTools(); });
  win.setPosition(0, 0);
}

app.on('ready', createWindow);